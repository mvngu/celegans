Neural network of the nematode C. Elegans
=========================================

Compiled by [WattsStrogatz1998]_ from original experimental data by
[WhiteEtAl1986]_.

The file celegansneural.gml describes a weighted, directed network
representing the neural network of C. Elegans.  The data were taken from
the web site of Prof. Duncan Watts at Columbia University,
http://cdg.columbia.edu/cdg/datasets.  The nodes in the original data were
not consecutively numbered, so they have been renumbered to be consecutive.
The original node numbers from Watts' data file are retained as the labels
of the nodes.  Edge weights are the weights given by Watts.

These data can be cited as:

  .. [WattsStrogatz1998]
     Duncan J. Watts and Steven H. Strogatz.  Collective dynamics of
     'small-world' networks.  *Nature*, 393(4):440--442, 1998,
     doi:10.1038/30918.

  .. [WhiteEtAl1986]
     J. G. White, E. Southgate, J. N. Thompson, and S. Brenner.  The
     Structure of the Nervous System of the Nematode Caenorhabditis
     elegans.  *Philosophical Transactions of The Royal Society B*,
     314(1165):1--340, 1986, doi:10.1098/rstb.1986.0056.
