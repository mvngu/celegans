/**************************************************************************
 * Copyright (C) 2013 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

#include <assert.h>
#include <igraph.h>
#include <stdio.h>

/* The edge list of the C. elegans neural network.  All self-loops are ignored.
 */
int main() {
  FILE *file;
  igraph_t G;
  igraph_vector_t d;
  igraph_vector_t gtypes, vtypes, etypes;
  igraph_strvector_t gnames, vnames, enames;
  long int i, j;

  igraph_i_set_attribute_table(&igraph_cattribute_table);

  file = fopen("celegansneural.gml", "r");
  igraph_read_graph_gml(&G, file);
  fclose(file);
  /* each node links to some other node */
  for (i = 0; i < igraph_vcount(&G); i++) {
    igraph_vector_init(&d, 0);
    igraph_degree(&G, &d, igraph_vss_1(i), IGRAPH_ALL, IGRAPH_NO_LOOPS);
    assert(VECTOR(d)[0] > 0);
    igraph_vector_destroy(&d);
  }

  /* get the node and edge attributes */
  igraph_vector_init(&gtypes, 0);
  igraph_vector_init(&vtypes, 0);
  igraph_vector_init(&etypes, 0);
  igraph_strvector_init(&gnames, 0);
  igraph_strvector_init(&vnames, 0);
  igraph_strvector_init(&enames, 0);

  igraph_cattribute_list(&G, &gnames, &gtypes, &vnames, &vtypes,
                         &enames, &etypes);

  /* node attributes; get the node weights only */
  file = fopen("node-weight.dat", "w");
  for (i = 0; i < igraph_vcount(&G); i++) {
    fprintf(file, "%li ", i);
    for (j = 0; j < igraph_strvector_size(&vnames); j++) {
      if (VECTOR(vtypes)[j] != IGRAPH_ATTRIBUTE_NUMERIC) {
        fprintf(file, "%s\n", VAS(&G, STR(vnames,j), i));
      }
    }
  }
  fclose(file);
  /* edge attributes; get the edge weights only */
  file = fopen("celegans.edge", "w");
  for (i = 0; i < igraph_ecount(&G); i++) {
    fprintf(file, "%li %li ",
            (long int)IGRAPH_FROM(&G, i), (long int)IGRAPH_TO(&G, i));
    for (j = 0; j < igraph_strvector_size(&enames); j++) {
      if (VECTOR(etypes)[j] == IGRAPH_ATTRIBUTE_NUMERIC) {
        fprintf(file, "%li\n", (long int)EAN(&G, STR(enames, j), i));
      }
    }
  }
  fclose(file);

  igraph_destroy(&G);
  igraph_strvector_destroy(&enames);
  igraph_strvector_destroy(&vnames);
  igraph_strvector_destroy(&gnames);
  igraph_vector_destroy(&etypes);
  igraph_vector_destroy(&vtypes);
  igraph_vector_destroy(&gtypes);

  return 0;
}
